<?php
/**
 * PHP version 7
 *
 * Pet Search Result
 *
 * @author      Webjump Core Team <dev@webjump.com.br>
 * @copyright   2021 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 * @link        http://www.webjump.com.br
 *
 */

namespace Webjump\WorkWithDatabase\Model;

use Magento\Framework\Api\SearchResults;
use Webjump\WorkWithDatabase\Api\Data\PetSearchResultInterface;

/**
 * Pet Search Result
 *
 * @author      Webjump Core Team <dev@webjump.com.br>
 * @copyright   2021 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 * @link        http://www.webjump.com.br
 *
 */
class PetSearchResult extends SearchResults implements PetSearchResultInterface
{

}

