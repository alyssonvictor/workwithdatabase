<?php
/**
 * PHP version 7
 *
 * Pet repository
 *
 * @author      Webjump Core Team <dev@webjump.com.br>
 * @copyright   2021 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 * @link        http://www.webjump.com.br
 *
 */

namespace Webjump\WorkWithDatabase\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use Webjump\WorkWithDatabase\Api\Data\PetRepositoryDataInterface;
use Webjump\WorkWithDatabase\Api\PetRepositoryInterface;
use Webjump\WorkWithDatabase\Model\PetFactory;
use Webjump\WorkWithDatabase\Model\ResourceModel\Pet\CollectionFactory;
use Webjump\WorkWithDatabase\Model\ResourceModel\Pet as PetResource;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use \Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\Search\SearchResultFactory;

/**
 * Pet repository
 *
 * @author      Webjump Core Team <dev@webjump.com.br>
 * @copyright   2021 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 * @link        http://www.webjump.com.br
 *
 */
class PetRepository implements PetRepositoryInterface
{

    /**
     * @var CollectionFactory
     */
    private CollectionFactory $_petCollectionFactory;

    /**
     * @var PetFactory
     */
    private PetFactory $_petFactory;

    /**
     * @var PetResource
     */
    private PetResource $_petResource;


    /**
     * @var CollectionProcessorInterface
     */
    private CollectionProcessorInterface $_collectionProcessor;

    /**
     * @var SearchResultFactory
     */
    private SearchResultFactory $_searchResultFactory;


    /**
     * @param CollectionFactory $petCollectionFactory
     * @param \Webjump\WorkWithDatabase\Model\PetFactory $petFactory
     * @param PetResource $petResource
     * @param CollectionProcessorInterface $collectionProcessor
     * @param SearchResultFactory $searchResultFactory
     */
    public function __construct(
        CollectionFactory $petCollectionFactory,
        PetFactory $petFactory,
        PetResource $petResource,
        CollectionProcessorInterface $collectionProcessor,
        SearchResultFactory $searchResultFactory
    )
    {
        $this->_petCollectionFactory = $petCollectionFactory;
        $this->_petFactory = $petFactory;
        $this->_petResource = $petResource;
        $this->_collectionProcessor = $collectionProcessor;
        $this->_searchResultFactory = $searchResultFactory;
    }

    /**
     * Method to save a new pet
     *
     * @param PetRepositoryDataInterface $pet
     *
     * @return PetRepositoryDataInterface
     */
    public function save(PetRepositoryDataInterface $pet)
    {
        $this->_petResource->save($pet);
        return $pet;
    }

    /**
     * Method to get specific pet
     *
     * @param int $id
     *
     * @return mixed|void
     */
    public function getById(int $id)
    {
        $pet = $this->_petFactory->create()->load($id);

        if (!$pet->getId()) {
            throw new NoSuchEntityException(__('Unable to find Student with ID "%1"', $id));
        }

        return $pet;
    }


    /**
     * Method to get all pets
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->_petCollectionFactory->create();
        $this->_collectionProcessor->process($searchCriteria, $collection);
        $searchResults = $this->_searchResultFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());

        return $searchResults;
    }
}
