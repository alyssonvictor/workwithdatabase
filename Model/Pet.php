<?php
/**
 * PHP version 7
 *
 * Pet model
 *
 * @author      Webjump Core Team <dev@webjump.com.br>
 * @copyright   2021 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 * @link        http://www.webjump.com.br
 *
 */

declare(strict_types=1);

namespace Webjump\WorkWithDatabase\Model;

use \Magento\Framework\Model\AbstractModel;
use Webjump\WorkWithDatabase\Model\ResourceModel\Pet as PetResource;
use Webjump\WorkWithDatabase\Api\Data\PetRepositoryDataInterface;

/**
 * Pet model
 *
 * @author      Webjump Core Team <dev@webjump.com.br>
 * @copyright   2021 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 * @link        http://www.webjump.com.br
 *
 */
class Pet extends AbstractModel implements PetRepositoryDataInterface
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(PetResource::class);
    }

    /**
     * @return array|mixed|string|null
     */
    public function getPetName()
    {
        return $this->_getData(self::PET_NAME);
    }

    /**
     * @param string $petName
     * @return $this|PetRepositoryDataInterface
     */
    public function setPetName(string $petName)
    {
       $this->setData(self::PET_NAME, $petName);
       return $this;
    }

    /**
     * @return mixed|string|null
     */
    public function getPetOwner()
    {
        return $this->_getData(self::PET_OWNER);
    }

    /**
     * @param string $petOwner
     * @return $this|PetRepositoryDataInterface
     */
    public function setPetOwner(string $petOwner)
    {
        $this->setData(self::PET_OWNER, $petOwner);
        return $this;
    }

    /**
     * @return mixed|string|null
     */
    public function getCreatedAt()
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * @return mixed|string|null
     */
    public function getOwnerTelephone()
    {
        return $this->_getData(self::OWNER_TELEPHONE);
    }

    /**
     * @param string $ownerTelephone
     * @return $this|PetRepositoryDataInterface
     */
    public function setOwnerTelephone(string $ownerTelephone)
    {
        $this->setData(self::OWNER_TELEPHONE, $ownerTelephone);
        return $this;
    }

    /**
     * @return int|mixed|null
     */
    public function getOwnerId()
    {
        return $this->_getData(self::OWNER_ID);
    }

    /**
     * @param int $ownerId
     * @return $this|PetRepositoryDataInterface
     */
    public function setOwnerId(int $ownerId)
    {
        $this->setData(self::OWNER_ID, $ownerId);
        return $this;
    }
}
