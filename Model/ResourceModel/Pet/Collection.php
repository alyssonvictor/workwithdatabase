<?php
/**
 * PHP version 7
 *
 * Collection for Pet
 *
 * @author      Webjump Core Team <dev@webjump.com.br>
 * @copyright   2021 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 * @link        http://www.webjump.com.br
 *
 */

declare(strict_types=1);

namespace Webjump\WorkWithDatabase\Model\ResourceModel\Pet;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Webjump\WorkWithDatabase\Model\ResourceModel\Pet as PetResource;
use Webjump\WorkWithDatabase\Model\Pet as PetModel;

/**
 * Collection for Pet
 *
 * @author      Webjump Core Team <dev@webjump.com.br>
 * @copyright   2021 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 * @link        http://www.webjump.com.br
 *
 */
class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(PetModel::class, PetResource::class);
    }
}
