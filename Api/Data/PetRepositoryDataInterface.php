<?php
/**
 * PHP version 7
 *
 * Pet data interface
 *
 * @author      Webjump Core Team <dev@webjump.com.br>
 * @copyright   2021 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 * @link        http://www.webjump.com.br
 *
 */

declare(strict_types=1);

namespace Webjump\WorkWithDatabase\Api\Data;

/**
 * Pet data interface
 *
 * @author      Webjump Core Team <dev@webjump.com.br>
 * @copyright   2021 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 * @link        http://www.webjump.com.br
 *
 */
interface PetRepositoryDataInterface
{
    /**
     * Const for entity_id column
     */
    const ENTITY_ID = 'entity_id';

    /**
     * Const for pet_name column
     */
    const PET_NAME = 'pet_name';

    /**
     * Const for pet_owner column
     */
    const PET_OWNER = 'pet_owner';

    /**
     * Const for created_at column
     */
    const CREATED_AT = 'created_at';

    /**
     * Const for owner_telephone column
     */
    const OWNER_TELEPHONE = 'owner_telephone';

    /**
     * Const for owner_id column
     */
    const OWNER_ID = 'owner_id';

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return self
     */
    public function setId(int $id);

    /**
     * @return string
     */
    public function getPetName();

    /**
     * @param string $petName
     * @return self
     */
    public function setPetName(string $petName);

    /**
     * @return string
     */
    public function getPetOwner();

    /**
     * @param string $petOwner
     * @return self
     */
    public function setPetOwner(string $petOwner);

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @return string
     */
    public function getOwnerTelephone();

    /**
     * @param string $ownerTelephone
     * @return self
     */
    public function setOwnerTelephone(string $ownerTelephone);

    /**
     * @return int
     */
    public function getOwnerId();

    /**
     * @param int $ownerId
     * @return self
     */
    public function setOwnerId(int $ownerId);
}
