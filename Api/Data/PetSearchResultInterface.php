<?php
/**
 * PHP version 7
 *
 * Pet search result interface
 *
 * @author      Webjump Core Team <dev@webjump.com.br>
 * @copyright   2021 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 * @link        http://www.webjump.com.br
 *
 */

namespace Webjump\WorkWithDatabase\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Pet search result interface
 *
 * @author      Webjump Core Team <dev@webjump.com.br>
 * @copyright   2021 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 * @link        http://www.webjump.com.br
 *
 */
interface PetSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return \Webjump\WorkWithDatabase\Api\Data\PetRepositoryDataInterface[]
     */
    public function getItems();

    /**
     * @param \Webjump\WorkWithDatabase\Api\Data\PetRepositoryDataInterface[] $items
     * @return void
     */
    public function setItems(array $items);
}
