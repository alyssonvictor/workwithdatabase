<?php
/**
 * PHP version 7
 *
 * Pet interface
 *
 * @author      Webjump Core Team <dev@webjump.com.br>
 * @copyright   2021 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 * @link        http://www.webjump.com.br
 *
 */

declare(strict_types=1);

namespace Webjump\WorkWithDatabase\Api;

use \Magento\Framework\Api\SearchCriteriaInterface;
use \Webjump\WorkWithDatabase\Api\Data\PetRepositoryDataInterface;
use \Webjump\WorkWithDatabase\Api\Data\PetSearchResultInterface;

/**
 * Pet interface
 *
 * @author      Webjump Core Team <dev@webjump.com.br>
 * @copyright   2021 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 * @link        http://www.webjump.com.br
 *
 */
interface PetRepositoryInterface
{
    /**
     * Method to save a new pet
     *
     * @param \Webjump\WorkWithDatabase\Api\Data\PetRepositoryDataInterface $pet
     *
     * @return \Webjump\WorkWithDatabase\Api\Data\PetRepositoryDataInterface
     */
    public function save(PetRepositoryDataInterface $pet);

    /**
     * Method to get specific pet
     *
     * @param int $id
     *
     * @return \Webjump\WorkWithDatabase\Api\Data\PetRepositoryDataInterface
     */
    public function getById(int $id);

    /**
     * Method to get all pets
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     *
     * @return \Webjump\WorkWithDatabase\Api\Data\PetSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
