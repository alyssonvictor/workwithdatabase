<?php

namespace Webjump\WorkWithDatabase\Setup\Patch;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Psr\Log\LoggerInterface;
use Webjump\WorkWithDatabase\Model\PetFactory;
use Webjump\WorkWithDatabase\Model\PetRepository;

class CreatePets implements DataPatchInterface
{
    /**
     * @var PetFactory $_petFactory
     */
    private PetFactory $_petFactory;

    /**
     * @var PetRepository $_petRepository
     */
    private PetRepository $_petRepository;

    private LoggerInterface $_logger;

    public function __construct(
        PetFactory $petFactory,
        PetRepository $petRepository,
        LoggerInterface $logger
    )
    {
        $this->_petFactory = $petFactory;
        $this->_petRepository = $petRepository;
        $this->_logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $pet1 = $this->_petFactory->create();
        $pet2 = $this->_petFactory->create();
        $pet3 = $this->_petFactory->create();

        $pet1->setPetName("Lessy");
        $pet1->setPetOwner("Alysson Victor");
        $pet1->setOwnerTelephone("43 996739858");
        $pet1->setOwnerId(1);

        $pet2->setPetName("Luna");
        $pet2->setPetOwner("Alysson Victor");
        $pet2->setOwnerTelephone("43 996739858");
        $pet2->setOwnerId(1);

        $pet3->setPetName("Iorek");
        $pet3->setPetOwner("Alysson Victor");
        $pet3->setOwnerTelephone("43 996739858");
        $pet3->setOwnerId(1);

        $this->_petRepository->save($pet1);
        $this->_petRepository->save($pet2);
        $this->_petRepository->save($pet3);
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
